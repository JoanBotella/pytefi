import pytefi
import demo

class AppRunner(pytefi.AppRunnerAbs):


	def _buildApp(self):
		return demo.App()


	def _buildStartStatusBuilder(self):
		return demo.StartStatusBuilder()