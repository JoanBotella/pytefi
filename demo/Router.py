import pytefi
import demo.node

class Router(pytefi.RouterAbs):


	def _buildNodes(self):
		return [
			demo.node.TitleMenu(),
			demo.node.Bedroom(),
			demo.node.Corridor()
		]
