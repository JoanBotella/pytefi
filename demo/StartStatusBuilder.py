import pytefi
import demo

class StartStatusBuilder():


	def build(self):
		status = pytefi.Status()

		status.enteringNode = True
		status.nodeCode = demo.node.TitleMenu().getCode()
		status.variables = demo.Variables()

		return status