import pytefi
import demo.node

class Bedroom(pytefi.LocationNodeAbs):


	def getCode(self):
		return 'bedroom'


	def _getTitle(self):
		return 'Bedroom'


	def run(self, request):
		response = super().run(request)

		if response.matched:
			return response

		if request.command == 'look':
			return self.__runLook(response)

		if request.command == 'look shoes':
			return self.__runLookShoes(response)

		if request.command == 'smell':
			return self.__runSmell(response)

		if request.command == 'smell shoes':
			return self.__runSmellShoes(response)

		if request.command == 'go corridor':
			return self.__runGoCorridor(response)

		return response


	def __runLook(self, response):
		response.appendSlide(text = 'This is my bedroom. My <item>shoes</item> are under the bed.<br />I can exit to the <exit>corridor</exit>.')
		return response


	def __runLookShoes(self, response):
		response.appendSlide(text = 'I played football with them yesterday.')
		return response


	def __runSmell(self, response):
		slideText = 'Smells funny.'

		if response.status.variables.shoesSmelled == True:
			slideText += ' It comes from my <item>shoes</item>.'

		response.appendSlide(text = slideText)
		return response


	def __runSmellShoes(self, response):
		if response.status.variables.shoesSmelled == True:
			response.appendSlide(text = 'They smell like cheese.')
		else:
			response.appendSlide(text = 'Oh my, that smells like...')
			response.appendSlide(text = 'Cheese!')
			response.status.variables.shoesSmelled = True
		return response


	def __runGoCorridor(self, response):
		response.appendSlide(text = 'The truth is out there.')
		response.changeNode(demo.node.Corridor().getCode())
		return response