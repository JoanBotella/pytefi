import pytefi
import demo.node

class Corridor(pytefi.LocationNodeAbs):


	def getCode(self):
		return 'corridor'


	def _getTitle(self):
		return 'Corridor'


	def run(self, request):
		response = super().run(request)

		if response.matched:
			return response

		if request.command == 'look':
			return self.__runLook(response)

		if request.command == 'go bedroom':
			return self.__runGoBedroom(response)

		return response


	def __runLook(self, response):
		response.appendSlide(text = 'A very long corridor.<br />I can enter my <exit>bedroom</exit>.')
		return response


	def __runGoBedroom(self, response):
		response.changeNode(demo.node.Bedroom().getCode())
		return response