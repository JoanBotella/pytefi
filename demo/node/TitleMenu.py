import pytefi
import demo.node

class TitleMenu(pytefi.NodeAbs):


	def getCode(self):
		return 'title_menu'


	def _getTitle(self):
		return 'Title Menu'


	def run(self, request):
		response = super().run(request)

		if response.matched:
			return response

		if request.command == 'look':
			return self.__runLook(response)

		if request.command == '1':
			return self.__run1(response)

		if request.command == '2':
			return self.__run2(response)

		return response


	def __runLook(self, response):
		response.appendSlide(text = 'Welcome to the PyTeFi Demo game. Please, choose an option:<br /><br />1) Start game<br />2) Quit')
		return response


	def __run1(self, response):
		response.changeNode(demo.node.Bedroom().getCode())
		return response


	def __run2(self, response):
		response.status.quit = True
		return response
