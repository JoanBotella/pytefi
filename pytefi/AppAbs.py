import abc
import pytefi

class AppAbs(abc.ABC):


	def __init__(self):
		self.__router = self._buildRouter()
		self.__systemResponder = self._buildSystemResponder()


	@abc.abstractmethod
	def _buildRouter(self):
		pass


	def _buildSystemResponder(self):
		return pytefi.SystemResponder()


	def run(self, request):

		node = self.__router.route(request.status.nodeCode)

		response = node.run(request)

		if not response.matched:
			response = self.__systemResponder.run(request)

		return response