import abc
import pytefi

class AppRunnerAbs(abc.ABC):


	def __init__(self):
		self.__requestBuilder = self._buildRequestBuilder()
		self.__app = self._buildApp()
		self.__responseDispatcher = self._buildResponseDispatcher()


	def _buildRequestBuilder(self):
		return pytefi.RequestBuilder()


	@abc.abstractmethod
	def _buildApp(self):
		pass


	def _buildResponseDispatcher(self):
		return pytefi.ResponseDispatcher()


	def run(self):
		status = self._buildStartStatus()

		try:
			while not status.quit:
				status = self.__step(status)
		except KeyboardInterrupt:
			pass


	def _buildStartStatus(self):
		builder = self._buildStartStatusBuilder()
		return builder.build()


	@abc.abstractmethod
	def _buildStartStatusBuilder(self):
		pass


	def __step(self, status):

		request = self.__requestBuilder.build(status)

		response = self.__app.run(request)

		self.__responseDispatcher.dispatch(response)

		return response.status
