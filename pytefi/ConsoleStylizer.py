
class ConsoleStylizer:

	# https://www.skillsugar.com/how-to-print-coloured-text-in-python

	__FG_GREY = '\033[90m'
	__FG_RED = '\033[91m'
	__FG_GREEN = '\033[92m'
	__FG_YELLOW = '\033[93m'
	__FG_BLUE = '\033[94m'
	__FG_PINK = '\033[95m'
	__FG_TURQUOISE = '\033[96m'

	__BG_GRAY = '\033[100m'
	__BG_RED = '\033[101m'
	__BG_GREEN = '\033[102m'
	__BG_YELLOW = '\033[103m'
	__BG_BLUE = '\033[104m'
	__BG_PINK = '\033[105m'
	__BG_TURQUOISE = '\033[106m'

	__STYLE_RESET = '\033[0m'
	__STYLE_BOLD = '\033[1m'
	__STYLE_FAINT = '\033[2m'
	__STYLE_ITALIC = '\033[3m'
	__STYLE_UNDERLINE = '\033[4m'
	__STYLE_BLINK = '\033[5m'
	__STYLE_CONCEAL = '\033[8m'
	__STYLE_STRIKE = '\033[9m'
	__STYLE_OVERLINE = '\033[53m'


	def stylize(self, text):
		text = text.replace('<br />', '\n')
		text = text.replace('<title>', '\n' + self.__STYLE_BOLD)
		text = text.replace('</title>', self.__STYLE_RESET)
		text = text.replace('<item>', self.__FG_BLUE)
		text = text.replace('</item>', self.__STYLE_RESET)
		text = text.replace('<exit>', self.__FG_RED)
		text = text.replace('</exit>', self.__STYLE_RESET)
		return text
