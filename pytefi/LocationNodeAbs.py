import pytefi

class LocationNodeAbs(pytefi.NodeAbs):


	def run(self, request):
		response = super().run(request)

		request.status.location = self.getCode()

		return response
