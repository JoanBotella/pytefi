import abc
import pytefi

class NodeAbs(abc.ABC):


	def run(self, request):
		response = pytefi.Response(request.status)

		if response.status.enteringNode:
			response.title = self._getTitle()
			response.status.enteringNode = False

		return response


	@abc.abstractmethod
	def _getTitle(self):
		pass


	@abc.abstractmethod
	def getCode(self):
		pass