import pytefi
import re

class RequestBuilder():


	def __init__(self):
		self.__nodePrompt = self._buildNodePrompt()


	def build(self, status):

		command = self.__buildCommand(status)

		return pytefi.Request(status, command)


	def __buildCommand(self, status):
		if status.enteringNode:
			return 'look'

		raw = input(self.__nodePrompt)

		return self.__normalizeCommand(raw)


	def _buildNodePrompt(self):
		stylizer = pytefi.ConsoleStylizer()
		return stylizer.stylize('> ')


	def __normalizeCommand(self, r):

		r = r.strip()
		r = r.lower()
		r = r.replace('  ', ' ')
		r = r.replace(' a ', ' ')
		r = r.replace(' the ', ' ')
		r = re.sub(r'\s+', ' ', r)

		r = re.sub('^look at', 'look', r)
		r = re.sub('^stare( at)?', 'look', r)

		r = re.sub('^go to', 'go', r)
		r = re.sub('^exit to', 'go', r)
		r = re.sub('^walk( to)?', 'go', r)
		r = re.sub('^run( to)?', 'go', r)

		r = re.sub('^pick up', 'take', r)
		r = re.sub('^grab', 'take', r)

		if r == 'exit':
			return 'quit'

		return r