import pytefi

class Response:


	def __init__(self, status):
		self.status = status
		self.matched = False
		self.title = None
		self.slides = []


	def appendSlide(self, text = None):
		self.matched = True
		slide = pytefi.Slide()
		slide.text = text
		self.slides.append(slide)


	def changeNode(self, nodeCode):
		self.matched = True
		self.status.nodeCode = nodeCode
		self.status.enteringNode = True