import pytefi

class ResponseDispatcher():


	def __init__(self):
		self.__stylizer = pytefi.ConsoleStylizer()


	def dispatch(self, response):

		if response.title:
			print(self.__stylizeTitle(response.title))

		index = 0
		slidesLength = len(response.slides)
		while index < slidesLength:

			if index > 0:
				self._showSlidePrompt()

			self.__dispatchSlide(response.slides[index])

			index += 1

		if slidesLength > 0 and response.status.enteringNode:
				self._showSlidePrompt()


	def _showSlidePrompt(self):
		input(self._getSlidePrompt())


	def _getSlidePrompt(self):
		return self.__stylizer.stylize('[...]')


	def __dispatchSlide(self, slide):
		print(self.__stylizer.stylize('<br />' + slide.text + '<br />'))


	def __stylizeTitle(self, title):
		return self.__stylizer.stylize('<title>' + title + '</title>')