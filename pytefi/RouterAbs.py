import abc

class RouterAbs(abc.ABC):


	def __init__(self):
		self.nodes = self._buildNodes()


	@abc.abstractmethod
	def _buildNodes(self):
		pass


	def route(self, code):

		for node in self.nodes:
			if node.getCode() == code:
				return node

		raise Exception('Node code not found')