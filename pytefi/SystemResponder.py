import pytefi

class SystemResponder():


	def run(self, request):

		response = pytefi.Response(request.status)

		if request.command == 'quit' or response.status.quit:
			response.appendSlide('Bye!')
			response.status.quit = True
			return response

		response.appendSlide(text = 'I do not understand that.')

		return response
